<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data = [
			"keybarang" => $this->db->get("barang")->result_array()
		];
		$this->load->view('barang', $data);
		// echo json_encode($data['keybarang'], JSON_PRETTY_PRINT);
	}

	public function tambah()
	{
		$this->form_validation->set_rules("nama", "Nama", "required");
		$this->form_validation->set_rules("harga", "Harga", "required|numeric");

		if ($this->form_validation->run() == False) {
			$this->load->view('tambah_barang');
		} else {
			$data = [
				"nama" => $this->input->post("nama", true),
				"harga" => $this->input->post("harga", true)
			];
			$this->db->insert("barang", $data);
			$this->session->set_flashdata("msg", 'Data Berhasil Ditambah');
			redirect(base_url("index.php/welcome/index"));
		}
	}
	
	public function update($id)
	{
		$this->form_validation->set_rules("nama", "Nama", "required");
		$this->form_validation->set_rules("harga", "Harga", "required|numeric");

		if ($this->form_validation->run() == False) {
			$data = [
				"barang" => $this->db->get_where("barang", ['id' => $id])->row_array()
			];
			$this->load->view('ubah_barang', $data);
		} else {
			$data = [
				"nama" => $this->input->post("nama", true),
				"harga" => $this->input->post("harga", true)
			];
			$this->db->update("barang", $data, ['id' => $id]);
			$this->session->set_flashdata("msg", 'Data Berhasil Diubah');
			redirect(base_url("index.php/welcome/index"));
		}
	}
	
	public function delete($id)
	{
		$this->db->delete("barang", ['id' => $id]);
		$this->session->set_flashdata("msg", 'Data Berhasil Dihapus');
		redirect(base_url("index.php/welcome/index"));
	}

	public function excel()
	{
		// $barang = $this->db->get_where("barang", ['id' => $id])->row_array();
		// $barang['peminjam'] = $this->db->get_where("peminjam", ['id_barang' => $id])->result_array();
		// var_dump($barang);
		$this->load->view("excel");
	}
}
